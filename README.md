# World_Population_Dashboard_R


#  World Population Data: Interactive dashboard

Interactive dashbaord created with RMarkdown. 

#  Used Libraries and packages in This Project

- library(flexdashboard) # Dashboard package
- library(highcharter) # Interactive data visualizations
-l ibrary(plotly) # Interactive data visualizations
- library(viridis) # Color gradients
- library(tidyverse) # Metapackge
- library(countrycode) # Converts country names/codes
- library(rjson) # JSON reader
- library(crosstalk) # Provides interactivity for HTML widgets
- library(DT) # Displaying data tables
- library(lubridate)

#  References

If you want to work with the dashboard, all you need to do is to download and run the `WorldPopulationDashboard.rmd` file. 
The dashboard Data can be viewed on kaggle at [https://www.kaggle.com/datasets/farzamajili/world-population](https://www.kaggle.com/datasets/farzamajili/world-population). 

#  Watch the Video To See the Dashboard In Action! 

![World_Population_Data_Dashboard_-_Google_Chrome_2023-02-10_16-29-34](/uploads/61efcd1c6f4e414cc5fb7c3e6a5c3577/World_Population_Data_Dashboard_-_Google_Chrome_2023-02-10_16-29-34.mp4)
